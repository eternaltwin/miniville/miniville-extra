This repository contains the original assets from the orginal Miniville MT's game (http://miniville.fr + http://antiville.fr)

# Miniville & antiville
Le jeu Miniville est séparé en 2 sites :
* miniville.fr => pour créer et agrandir sa ville. Les clics sur les différents liens de parrainage Miniville.fr permettent d'agrandir sa ville.
* antiville.fr => pour envoyer des missiles sur des villes concurrentes. Chaque clic sur le lien de parrainage Antiville.fr construit une partie supplémentaire du missile. Quand le missile est achevé, il est envoyé sur la ville et en détruit une partie.


### Contenu du dépôt Git
**Fichier "client.swf"** : fichier Flash principal : 
	> Dans Miniville.fr, affiche la ville.
	> Dans Antiville.fr, affiche la ville à travers un filtre rouge + en surimpression, le compteur de signatures manquantes avant lancement du missile.

"Miniville" and "Antiville" share the same SWF client, the only difference is the parameter "anti" which makes display the countdown of signatures (source: Booti on Discord).


**Dossier /original-swf : tous les fichiers Flash du jeu original de MT :**
  - client.swf : affiche la miniville
  - name.swf : affiche le nom de la ville dans un panneau d'agglomération français (rectangle blanc à contour rouge). S'affiche dans la bannière du site.
  - name_anti.swf [Flash] : affiche le nom de la ville visée dans un panneau routier directionnel (flèche blanche à contour noir)
  - ranking.swf : affiche le classement des villes.
  - silo.swf [Flash] : affiche le silo à missile en construction (pour antiville)

**Dossier /desobfuscated-swf : les fichiers Flash décompilés**
(contiennent notamment des images du jeu)
Décompilé par Azaml (yuuty) sur Discord

**Dossier /original-sites-pages : pages HTML téléchargées du jeu original**
Contient le HTML, le javascript et diverses ressources du site.


### Exploitation des SWF
Ouverts directement dans le navigateur, les SWF n'affichent rien. Il manque des paramètres d'appel (a priori, au minimum l'identifiant de la ville à afficher).

Les SWF sont appelés à travers "SWFObject", une librairie qui teste si le lecteur Flash est installé. Cette librairie est abandonnée mais son code est sur Github : https://github.com/swfobject/swfobject

